# Loper

[tbfisher](https://www.drupal.org/u/tbfisher)'s developer toolkit.

## Functions

### `loper_dpq($query, $return = TRUE, $name = NULL)`

Wrapper around devel dpq().

Replaces table placeholders with table name. Runs for any user, changes $return default to TRUE.

## Drush Commands

### `loper-search-fields`

Executes a search query on all entity field text attributes.

## Views

### Watchdog

Uses the [Views Watchdog](https://www.drupal.org/project/views_watchdog) module to add an enhanced log viewer.
