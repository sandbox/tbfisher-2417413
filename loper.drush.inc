<?php

/**
 * @file
 * Drush commands for loper module.
 */

/**
 * Imlements hook_drush_command().
 */
function loper_drush_command() {
  $items = array();

  $items['loper-search-fields'] = array(
    'description' => "Executes a search query on all entity field text attributes.",
    'arguments' => array(
      'value' => 'SQL search string.',
      'operator' => 'SQL search operator, defaults to "=".',
    ),
    'required-arguments' => 1,
    'options' => array(
      'format' => array(
        'description' => 'Output format.',
        'example-value' => 'json,csv',
      ),
      'match' => array(
        'description' => 'Include field value. Decreases performance.',
      ),
      'query' => array(
        'description' => 'Include sql query used to find a match.',
      ),
      // @todo
      // 'case-insensitive' => array(),
    ),
    'examples' => array(
      'drush lfsearch value' => 'Do a "= \'value\'" query',
      'drush lfsearch %keyword% like' => 'Do a "LIKE \'%keyword%\'" query',
    ),
    'aliases' => array('lfsearch'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
  );

  return $items;
}

/**
 * Command callback. Provides information from the 'Status Reports' admin page.
 */
function drush_loper_search_fields($value, $operator = '=') {
  return loper_search_fields_text($value, $operator,
    drush_get_option('format', 'json'),
    drush_get_option('match', FALSE),
    drush_get_option('query', FALSE));
}
