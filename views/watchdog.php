<?php

$view = new view();
$view->name = 'loper_watchdog';
$view->description = 'Uses Watchdog Views module to generate view of watchdog entries.';
$view->tag = 'Loper';
$view->base_table = 'watchdog';
$view->human_name = 'Watchdog';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'access site reports';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '25';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'wid' => 'wid',
  'hostname' => 'hostname',
  'link' => 'link',
  'location' => 'location',
  'message' => 'message',
  'referer' => 'referer',
  'severity' => 'severity',
  'timestamp' => 'timestamp',
  'type' => 'type',
);
$handler->display->display_options['style_options']['default'] = 'wid';
$handler->display->display_options['style_options']['info'] = array(
  'wid' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'hostname' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'link' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'location' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'message' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'referer' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'severity' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'timestamp' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'type' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Field: Watchdog: Wid */
$handler->display->display_options['fields']['wid']['id'] = 'wid';
$handler->display->display_options['fields']['wid']['table'] = 'watchdog';
$handler->display->display_options['fields']['wid']['field'] = 'wid';
$handler->display->display_options['fields']['wid']['separator'] = '';
/* Field: Watchdog: Timestamp */
$handler->display->display_options['fields']['timestamp']['id'] = 'timestamp';
$handler->display->display_options['fields']['timestamp']['table'] = 'watchdog';
$handler->display->display_options['fields']['timestamp']['field'] = 'timestamp';
$handler->display->display_options['fields']['timestamp']['date_format'] = 'custom';
$handler->display->display_options['fields']['timestamp']['custom_date_format'] = 'Y_m_d G:i:s';
$handler->display->display_options['fields']['timestamp']['second_date_format'] = 'long';
/* Field: Watchdog: Type */
$handler->display->display_options['fields']['type']['id'] = 'type';
$handler->display->display_options['fields']['type']['table'] = 'watchdog';
$handler->display->display_options['fields']['type']['field'] = 'type';
/* Field: Watchdog: Severity */
$handler->display->display_options['fields']['severity']['id'] = 'severity';
$handler->display->display_options['fields']['severity']['table'] = 'watchdog';
$handler->display->display_options['fields']['severity']['field'] = 'severity';
$handler->display->display_options['fields']['severity']['watchdog_severity_icon'] = 1;
/* Field: Watchdog: Message */
$handler->display->display_options['fields']['message']['id'] = 'message';
$handler->display->display_options['fields']['message']['table'] = 'watchdog';
$handler->display->display_options['fields']['message']['field'] = 'message';
$handler->display->display_options['fields']['message']['watchdog_message_format'] = 1;
$handler->display->display_options['fields']['message']['watchdog_message_link'] = 1;
/* Field: Watchdog: Hostname */
$handler->display->display_options['fields']['hostname']['id'] = 'hostname';
$handler->display->display_options['fields']['hostname']['table'] = 'watchdog';
$handler->display->display_options['fields']['hostname']['field'] = 'hostname';
/* Field: Watchdog: Location */
$handler->display->display_options['fields']['location']['id'] = 'location';
$handler->display->display_options['fields']['location']['table'] = 'watchdog';
$handler->display->display_options['fields']['location']['field'] = 'location';
/* Field: Watchdog: Referrer */
$handler->display->display_options['fields']['referer']['id'] = 'referer';
$handler->display->display_options['fields']['referer']['table'] = 'watchdog';
$handler->display->display_options['fields']['referer']['field'] = 'referer';
/* Field: Watchdog: Link */
$handler->display->display_options['fields']['link']['id'] = 'link';
$handler->display->display_options['fields']['link']['table'] = 'watchdog';
$handler->display->display_options['fields']['link']['field'] = 'link';
/* Filter criterion: Watchdog: Wid */
$handler->display->display_options['filters']['wid']['id'] = 'wid';
$handler->display->display_options['filters']['wid']['table'] = 'watchdog';
$handler->display->display_options['filters']['wid']['field'] = 'wid';
$handler->display->display_options['filters']['wid']['group'] = 1;
$handler->display->display_options['filters']['wid']['exposed'] = TRUE;
$handler->display->display_options['filters']['wid']['expose']['operator_id'] = 'wid_op';
$handler->display->display_options['filters']['wid']['expose']['label'] = 'Wid';
$handler->display->display_options['filters']['wid']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['wid']['expose']['operator'] = 'wid_op';
$handler->display->display_options['filters']['wid']['expose']['identifier'] = 'wid';
$handler->display->display_options['filters']['wid']['expose']['remember_roles'] = array(
  2 => '2',
  8 => 0,
  10 => 0,
  1 => 0,
  7 => 0,
  3 => 0,
  6 => 0,
  11 => 0,
  12 => 0,
  13 => 0,
  14 => 0,
);
/* Filter criterion: Watchdog: Timestamp */
$handler->display->display_options['filters']['timestamp']['id'] = 'timestamp';
$handler->display->display_options['filters']['timestamp']['table'] = 'watchdog';
$handler->display->display_options['filters']['timestamp']['field'] = 'timestamp';
$handler->display->display_options['filters']['timestamp']['group'] = 1;
$handler->display->display_options['filters']['timestamp']['exposed'] = TRUE;
$handler->display->display_options['filters']['timestamp']['expose']['operator_id'] = 'timestamp_op';
$handler->display->display_options['filters']['timestamp']['expose']['label'] = 'Timestamp';
$handler->display->display_options['filters']['timestamp']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['timestamp']['expose']['operator'] = 'timestamp_op';
$handler->display->display_options['filters']['timestamp']['expose']['identifier'] = 'timestamp';
$handler->display->display_options['filters']['timestamp']['expose']['remember_roles'] = array(
  2 => '2',
  8 => 0,
  10 => 0,
  1 => 0,
  7 => 0,
  3 => 0,
  6 => 0,
  11 => 0,
  12 => 0,
  13 => 0,
  14 => 0,
);
/* Filter criterion: Watchdog: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'watchdog';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['group'] = 1;
$handler->display->display_options['filters']['type']['exposed'] = TRUE;
$handler->display->display_options['filters']['type']['expose']['operator_id'] = 'type_op';
$handler->display->display_options['filters']['type']['expose']['label'] = 'Type';
$handler->display->display_options['filters']['type']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['type']['expose']['operator'] = 'type_op';
$handler->display->display_options['filters']['type']['expose']['identifier'] = 'type';
$handler->display->display_options['filters']['type']['expose']['remember_roles'] = array(
  2 => '2',
  8 => 0,
  10 => 0,
  1 => 0,
  7 => 0,
  3 => 0,
  6 => 0,
  11 => 0,
  12 => 0,
  13 => 0,
  14 => 0,
);
/* Filter criterion: Watchdog: Severity */
$handler->display->display_options['filters']['severity']['id'] = 'severity';
$handler->display->display_options['filters']['severity']['table'] = 'watchdog';
$handler->display->display_options['filters']['severity']['field'] = 'severity';
$handler->display->display_options['filters']['severity']['group'] = 1;
$handler->display->display_options['filters']['severity']['exposed'] = TRUE;
$handler->display->display_options['filters']['severity']['expose']['operator_id'] = 'severity_op';
$handler->display->display_options['filters']['severity']['expose']['label'] = 'Severity';
$handler->display->display_options['filters']['severity']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['severity']['expose']['operator'] = 'severity_op';
$handler->display->display_options['filters']['severity']['expose']['identifier'] = 'severity';
$handler->display->display_options['filters']['severity']['expose']['remember_roles'] = array(
  2 => '2',
  8 => 0,
  10 => 0,
  1 => 0,
  7 => 0,
  3 => 0,
  6 => 0,
  11 => 0,
  12 => 0,
  13 => 0,
  14 => 0,
);
/* Filter criterion: Watchdog: Message */
$handler->display->display_options['filters']['message']['id'] = 'message';
$handler->display->display_options['filters']['message']['table'] = 'watchdog';
$handler->display->display_options['filters']['message']['field'] = 'message';
$handler->display->display_options['filters']['message']['group'] = 1;
$handler->display->display_options['filters']['message']['exposed'] = TRUE;
$handler->display->display_options['filters']['message']['expose']['operator_id'] = 'message_op';
$handler->display->display_options['filters']['message']['expose']['label'] = 'Message';
$handler->display->display_options['filters']['message']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['message']['expose']['operator'] = 'message_op';
$handler->display->display_options['filters']['message']['expose']['identifier'] = 'message';
$handler->display->display_options['filters']['message']['expose']['remember_roles'] = array(
  2 => '2',
  8 => 0,
  10 => 0,
  1 => 0,
  7 => 0,
  3 => 0,
  6 => 0,
  11 => 0,
  12 => 0,
  13 => 0,
  14 => 0,
);
/* Filter criterion: Watchdog: Hostname */
$handler->display->display_options['filters']['hostname']['id'] = 'hostname';
$handler->display->display_options['filters']['hostname']['table'] = 'watchdog';
$handler->display->display_options['filters']['hostname']['field'] = 'hostname';
$handler->display->display_options['filters']['hostname']['group'] = 1;
$handler->display->display_options['filters']['hostname']['exposed'] = TRUE;
$handler->display->display_options['filters']['hostname']['expose']['operator_id'] = 'hostname_op';
$handler->display->display_options['filters']['hostname']['expose']['label'] = 'Hostname';
$handler->display->display_options['filters']['hostname']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['hostname']['expose']['operator'] = 'hostname_op';
$handler->display->display_options['filters']['hostname']['expose']['identifier'] = 'hostname';
$handler->display->display_options['filters']['hostname']['expose']['remember_roles'] = array(
  2 => '2',
  8 => 0,
  10 => 0,
  1 => 0,
  7 => 0,
  3 => 0,
  6 => 0,
  11 => 0,
  12 => 0,
  13 => 0,
  14 => 0,
);
$handler->display->display_options['filters']['hostname']['expose']['autocomplete_items'] = '10';
$handler->display->display_options['filters']['hostname']['expose']['autocomplete_min_chars'] = '0';
$handler->display->display_options['filters']['hostname']['expose']['autocomplete_field'] = 'hostname';
$handler->display->display_options['filters']['hostname']['expose']['autocomplete_raw_suggestion'] = 1;
$handler->display->display_options['filters']['hostname']['expose']['autocomplete_raw_dropdown'] = 1;
$handler->display->display_options['filters']['hostname']['expose']['autocomplete_dependent'] = 0;
/* Filter criterion: Watchdog: Location */
$handler->display->display_options['filters']['location']['id'] = 'location';
$handler->display->display_options['filters']['location']['table'] = 'watchdog';
$handler->display->display_options['filters']['location']['field'] = 'location';
$handler->display->display_options['filters']['location']['group'] = 1;
$handler->display->display_options['filters']['location']['exposed'] = TRUE;
$handler->display->display_options['filters']['location']['expose']['operator_id'] = 'location_op';
$handler->display->display_options['filters']['location']['expose']['label'] = 'Location';
$handler->display->display_options['filters']['location']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['location']['expose']['operator'] = 'location_op';
$handler->display->display_options['filters']['location']['expose']['identifier'] = 'location';
$handler->display->display_options['filters']['location']['expose']['remember_roles'] = array(
  2 => '2',
  8 => 0,
  10 => 0,
  1 => 0,
  7 => 0,
  3 => 0,
  6 => 0,
  11 => 0,
  12 => 0,
  13 => 0,
  14 => 0,
);
$handler->display->display_options['filters']['location']['expose']['autocomplete_items'] = '10';
$handler->display->display_options['filters']['location']['expose']['autocomplete_min_chars'] = '0';
$handler->display->display_options['filters']['location']['expose']['autocomplete_field'] = 'location';
$handler->display->display_options['filters']['location']['expose']['autocomplete_raw_suggestion'] = 1;
$handler->display->display_options['filters']['location']['expose']['autocomplete_raw_dropdown'] = 1;
$handler->display->display_options['filters']['location']['expose']['autocomplete_dependent'] = 0;
/* Filter criterion: Watchdog: Referrer */
$handler->display->display_options['filters']['referer']['id'] = 'referer';
$handler->display->display_options['filters']['referer']['table'] = 'watchdog';
$handler->display->display_options['filters']['referer']['field'] = 'referer';
$handler->display->display_options['filters']['referer']['group'] = 1;
$handler->display->display_options['filters']['referer']['exposed'] = TRUE;
$handler->display->display_options['filters']['referer']['expose']['operator_id'] = 'referer_op';
$handler->display->display_options['filters']['referer']['expose']['label'] = 'Referrer';
$handler->display->display_options['filters']['referer']['expose']['use_operator'] = TRUE;
$handler->display->display_options['filters']['referer']['expose']['operator'] = 'referer_op';
$handler->display->display_options['filters']['referer']['expose']['identifier'] = 'referer';
$handler->display->display_options['filters']['referer']['expose']['remember_roles'] = array(
  2 => '2',
  8 => 0,
  10 => 0,
  1 => 0,
  7 => 0,
  3 => 0,
  6 => 0,
  11 => 0,
  12 => 0,
  13 => 0,
  14 => 0,
);
$handler->display->display_options['filters']['referer']['expose']['autocomplete_items'] = '10';
$handler->display->display_options['filters']['referer']['expose']['autocomplete_min_chars'] = '0';
$handler->display->display_options['filters']['referer']['expose']['autocomplete_field'] = 'referer';
$handler->display->display_options['filters']['referer']['expose']['autocomplete_raw_suggestion'] = 1;
$handler->display->display_options['filters']['referer']['expose']['autocomplete_raw_dropdown'] = 1;
$handler->display->display_options['filters']['referer']['expose']['autocomplete_dependent'] = 0;

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page_1');
$handler->display->display_options['path'] = 'admin/reports/dblog_view';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Log Messages';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'management';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
$translatables['watchdog'] = array(
  t('Master'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('Wid'),
  t('.'),
  t('Timestamp'),
  t('Type'),
  t('Severity'),
  t('Message'),
  t('Hostname'),
  t('Location'),
  t('Referrer'),
  t('Link'),
  t('Page'),
);
